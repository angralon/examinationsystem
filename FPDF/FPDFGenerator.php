<?php
	require_once("../Controllers/GeneratePDFController.php");
	require('fpdf17/fpdf.php');
		
	class PDF extends FPDF
	{
		//Page header
		function Header(){
			$this->SetFont('Arial','B',15);
			//Move to the right
			$this->Cell(80);
			//Title
			$this->Cell(30,10,'Exam',1,0,'C');
			//Line break
			$this->Ln(20);
		}
		//Page footer
		function Footer(){
			//Position at 1.5 cm from bottom
			$this->SetY(-15);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}
	}
?>