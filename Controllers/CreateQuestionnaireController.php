<?php
	require_once("Controller.php");
	
	class CreateQuestionnaireController extends Controller {
		
		public function CreateQuestionnaireController() {
			parent::Controller();
		}
		
		/**
		 * @return the number of questions
		 */
		public function getNumberOfQuestions() {
			return intval(@$_GET['numberOfQuestions']);
		}
		
		/**
		 * @return the title of the questionnaire
		 */
		public function getTitle() {
			try {
				return $_GET['title'];
			}
			catch(Exception $e) {
				return "no title set!";
			}
		}
	}
?>