<?php
	require_once("../FPDF/FPDFGenerator.php");
	require_once("SeeStudentResultsController.php");
	
	class GeneratePDFController extends SeeStudentResultsController {
			
		public function GeneratePDFController() {
			$this->SeeStudentResultsController();
		}
		
		/**
		 * creates a new pdf 
		 */
		public function getPDF(){
			$pdf = new PDF();
			$pdf->AliasNbPages();
			$pdf->AddPage();
			$pdf->SetFont('Arial','',12);
			
			$myStudentID = $this->getStudentName();
			$myPoints = $this->getAddedPoints();
			$myMaxPoints = $this->getMaxPoints();		
			$myQuestionnaireTitle = $this->getQuestionnaireTitle();
			$myAnswers = $this->seeAnswers();
			$myMark = $this->getStudentMark();
			
			$pdf->Cell(40,10,"Questionnaire:");
			$pdf->Cell(40,10,$myQuestionnaireTitle);
			$pdf->Ln();
			$pdf->Cell(40,10,"Student Name:");
			$pdf->Cell(40,10,$myStudentID);
			$pdf->Ln();		
			$pdf->Cell(40,10,"Total Points:");
			$pdf->Cell(8,10,$myPoints." of ".$myMaxPoints);
			$pdf->Ln();
			$pdf->Cell(40,10,"Mark:");
			$pdf->Cell(40,10,$myMark);
			
			$pdf->AddPage();
			
			$pdf->Cell(40,10,'Question Number:');
			$pdf->Cell(40,10,'Student Answer:');
			$pdf->Cell(40,10,'Corresponding Points:');
			
			for ($i=0;$i<sizeof($myAnswers);$i++){
				$pdf->Ln();	
				$pdf->Cell(40,10,$myAnswers[$i]['questionNr']+1,1);
				$pdf->Cell(40,10,$myAnswers[$i]['studentAnswer'],1);			
				$pdf->Cell(40,10,$myAnswers[$i]['points'],1);
			}			
			
			$pdf->Output($myStudentID." ".$myQuestionnaireTitle.".pdf", 'I');
		}	
	}
?>