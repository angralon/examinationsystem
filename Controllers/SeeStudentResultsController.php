<?php
	require_once("Controller.php");
	
	class SeeStudentResultsController extends Controller {
		
		private $addedPoints,$maxPoints,$answers;
		
		public function SeeStudentResultsController() {
			$this->Controller();
			$this->getPointsForStudent();
		}
		
		/** 
		 * Gets the student- and the correct Answers of an exam and calculates the points 
		 */
		public function getPointsForStudent() {
			try {
				$answers = $this->queryHandler->query("
					SELECT sA.questionNr, sA.studentAnswer, a.correctAnswer, m.moduleCode, q.title, u.name, sE.examID 
					FROM studentExam sE
					INNER JOIN exam e ON sE.examID = e.examID
					INNER JOIN studentAnswer sA ON sE.studentExamID = sA.studentExamID
					INNER JOIN answer a ON (e.questionnaireID = a.questionnaireID AND sA.questionNr = a.questionNr)				
					INNER JOIN user u ON sE.studentID = u.userID
					INNER JOIN questionnaire q ON e.questionnaireID = q.questionnaireID
					INNER JOIN module m ON e.moduleID = m.moduleID
					WHERE sE.examID = '".$_REQUEST['examID']."' 
					AND sE.studentID = '".$_REQUEST['userID']."' 
				");
			}
			catch (Exception $e) {
				echo "Query error2!".$e->getMessage();
				require_once('../Views/footer.php');	
				exit;
			}
					
			$this->addedPoints = 0;
			$this->maxPoints = 0;
			for ($i=0;$i<sizeof($answers);$i++) {
				
				if($answers[$i]['studentAnswer'] == $answers[$i]['correctAnswer']) {
					$answers[$i]['points'] = 3;
				} 
				else if ($answers[$i]['studentAnswer'] == 0) {
					$answers[$i]['points'] = 0;
				}
				else {
					$answers[$i]['points'] = -1;
				} 
				$this->addedPoints += $answers[$i]['points'];
				$this->maxPoints += 3;
			}
			$this->answers = $answers;			
		}
		
		/**
		 * @return the mark for the calculation done in getPointsForStudent() 
		 */
		public function getStudentMark() {
			$factor = $this->getAddedPoints()/$this->getMaxPoints();
			if($factor >= 0.9) {
				return "A";			
			}
			if($factor >= 0.8) {
				return "B";			
			}
			if($factor >= 0.7) {
				return "C";			
			}
			if($factor >= 0.6) {
				return "D";			
			}
			if($factor >= 0.5) {
				return "E";			
			}
			if($factor >= 0.35) {
				return "Fx";			
			}
			else {
				return "F";			
			}
		}
		
		/** 
		 * Getters 
		 */
		public function getStudent() {
			return $_REQUEST['userID'];
		}
		public function getAddedPoints() {
			return $this->addedPoints;
		}
		public function getMaxPoints() {
			return $this->maxPoints;
		}
		public function getQuestionnaireTitle() {
			return $this->answers[0]['title'];
		}
		public function getStudentName() {
			return $this->answers[0]['name'];
		}
		public function getExamID() {
			return $this->answers[0]['examID'];
		}
		
		/**
		 * @return an assiociative and indexed array of answers and points for the student
		 */
		public function seeAnswers() {
			return $this->answers;
		}
	}
?>