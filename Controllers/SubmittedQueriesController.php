<?php
	require_once("Controller.php");
	
	/** 
	 * This Controller manages submitted forms and chooses what to do with the incoming post and get requests. 
	 * */
	class SubmittedQueriesController extends Controller {
		
		private $returnMessage;
		
		public function SubmittedQueriesController() {
			parent::Controller();
			$this->returnMessage = "";
			
			// Select the function to run
			$this->$_REQUEST['function']();
		}
		
		/** 
		 * If a new questionnaire was created, it is stored in here 
		 */
		public function storeQuestionnaire()	{
			
			// Check if the professor selected every radio button		
			for($i = 0; $i < $_POST['numberOfQuestions']; $i++) {
				if(@$_POST['q'.$i] < 1) {
					$this->returnMessage .= "Question ".($i+1)." is not selected";
					return;
				};
			}
			
			// Insert the questionnaire
			try {
			$insertId = $this->queryHandler->insert("INSERT INTO questionnaire (profID, title, numberOfQuestions) VALUES ('".parent::getUser()."', '".$_POST['title']."', '".$_POST['numberOfQuestions']."')");
			}
			catch (Exception $e) {
				$this->returnMessage .= "Insert error!";
				return;
			}
			
			// Insert the coresponding answers using the insertId of the questionnaire		
			for($i = 0; $i < $_POST['numberOfQuestions']; $i++) {
				try {
					$this->queryHandler->insert("INSERT INTO answer (questionnaireID,questionNr,correctAnswer) VALUES ('$insertId','$i','".$_POST['q'.$i]."')");
				}
				catch (Exception $e) {
					$this->returnMessage .= "Insert error!";
					return;
				}
			}
			
			// Set the return message that the view can get
			$this->returnMessage .= "Your questionnaire was stored";
		}
		
		/** 
		 * Starts an exam for a module 
		 * */
		public function activateQuestionnaire(){
			
			// Insert the exam	
			try {
				$insertId = $this->queryHandler->insert("INSERT INTO exam (moduleID, questionnaireID, start, duration, active) VALUES ('".$_POST['moduleID']."','".$_POST['questionnaireID']."', '".time()."', '".$_POST['duration']."', '1')");
			}
			catch (Exception $e) {
				$this->returnMessage .= "Insert error!";
				return;
			}
			
			// Set the return message that the view can get
			$this->returnMessage .= "Your exam was stored.";
		}
		
		/** 
		 * Terminates an exam. 
		 * Students can still submit their exam even if it's terminated, but they won't see it in their list of exams 
		 */
		public function endExam() {
			try {
				$this->queryHandler->exec("UPDATE exam SET active='0' WHERE examID='".$_REQUEST['examID']."'");
			}
			catch (Exception $e) {
				$this->returnMessage .= "Query error!";
				return;
			}
			
			// Set the return message that the view can get
			$this->returnMessage .= "This exam has been terminated.";
		}
		
		// Is used when a student submits his exam
		public function fillExam() {
			// Check if already filled out
			try {
				$rows = $this->queryHandler->query("
					SELECT studentExamID 
					FROM studentExam 
					WHERE examID = '".$_REQUEST['examID']."'
					AND studentID = '".$this->getUser()."'
				");		
			}
			catch (Exception $e) {
				$this->returnMessage .= "Query error!<br />";
				return;
			}
			if(sizeof($rows) > 0) {
				$this->returnMessage .= "You already stored your answers for this exam!";
				return;
			}
			
			// INSERT the studentExam
			try {
				$insertId = $this->queryHandler->insert("
					INSERT INTO studentExam 
					(examID, studentID, submitTime) VALUES 
					('".$_POST['examID']."','".$this->getUser()."', '".time()."')
				");
			}
			catch (Exception $e) {
				$this->returnMessage .= "Insert error!";
				return;
			}
			
			// INSERT the answers		
			for($i = 0; $i < $_POST['numberOfQuestions']; $i++) {
				try {
					$this->queryHandler->insert("
						INSERT INTO studentAnswer 
						(studentExamID, questionNr, studentAnswer) VALUES 
						('".$insertId."', '".$i."', '".@$_REQUEST['q'.$i]."')
					");
				}
				catch (Exception $e) {
					$this->returnMessage .= "Insert error!";
					return;
				}	
			}
			
			// Set the return message that the view can get
			$this->returnMessage .= "Your exam was stored";
		}
		
		/**
		 * @return the return message
		 */
		public function getReturnMessage() {
			return $this->returnMessage;
		}
	}
?>