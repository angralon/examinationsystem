<?php
	require_once("Controller.php");
	
	class SeeAnswersController extends Controller {
		
		public function SeeAnswersController() {
			parent::Controller();
		}
		
		/**
		 * @return the question number and the correct answer of the questionnaire
		 */
		public function seeAnswers(){
			try {
				return $this->queryHandler->query("SELECT questionNr,correctAnswer FROM answer WHERE questionnaireID='".$_GET['questionnaireID']."' ");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}
		}
		
		/**
		 * @return the title of the questionnaire
		 */
		public function getQuestionnaireTitle() {
			try {
				return $this->queryHandler->query("SELECT title FROM questionnaire WHERE questionnaireID='".$_GET['questionnaireID']."' ");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}
		}
		
		/**
		 * @return the id of the questionnaire
		 */
		public function getQuestionnaireID() {
			return $_GET['questionnaireID'];
		}		
	}
?>