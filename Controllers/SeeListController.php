<?php
	require_once("Controller.php");
	
	class SeeListController extends Controller {
		
		public function SeeListController() {
			parent::Controller();
		}
		
		/**
		 * Gets the useres of a module
		 * @return them as array
		 */
		public function seeUsers(){
			try {
				return $this->queryHandler->query("
					SELECT u.userID AS Id, u.email AS Email, u.name AS Name
					FROM user u
					INNER JOIN studentModule sm ON u.userID = sm.studentID
					INNER JOIN module m ON sm.moduleID = m.moduleID
					WHERE m.moduleID = '".$_GET['module']."'
				");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}			}
				 
		 /**
		 * Gets all the modules
		 * @return them as array
		 */
		public function seeModules(){
			try {
				return $this->queryHandler->query("SELECT * FROM module");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}
		}
	}
?>