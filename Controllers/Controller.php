<?php
	class Controller {
		
		protected $queryHandler;
		private $isLoggedIn = false;
		
		/**
		 * @param $checkSession sets if the controller should check whether the user is logged in or not
		 * @param $redirect sets fals if login redirection is unnecessary
		 */
		public function Controller($checkSession = true, $redirect = true) {			
			if(!$checkSession) {
				$this->requireAllClasses();
				$this->queryHandler = new QueryHandler();
			}
			else {
				$this->requireAllClasses();
				$this->checkSession();
				$this->queryHandler = new QueryHandler();
			}
			
			if($redirect) {
				$this->redirectIfNoLogin();
			}
		}
		
		/**
		 * loads the connector and the queryhandler into the file 
		 */
		private function requireAllClasses(){
	
			require_once("../SQL/Connector.php");
			require_once("../SQL/QueryHandler.php");
		}
		
		/**
		 * sets isLoggedIn to true if the session exists
		 */
		public function checkSession() {
			
			@session_start (); 
			if (isset($_SESSION["userID"])) 
			{ 
				$this->isLoggedIn = true;
			}
		}
		
		/**
		 * @return the boolean isLoggedIn
		 */
		public function isLoggedIn() {
			return $this->isLoggedIn;
		}
		
		/**
		 * @return true if a professor is logged in
		 */
		public function isProf() {
			if($_SESSION["prof"] == 1) {
				return true;
			}
			else {
				return false;	
			}
		}
		
		/**
		 * @return the user id of the active user
		 */
		public function getUser() {
			return $_SESSION["userID"];
		}
		
		/**
		 * goes back to the index page if the actual user is not logged in
		 */
		public function redirectIfNoLogin() {
			if(!$this->isLoggedIn()){
				header( 'Location: index.php' ) ;
				exit;
			}
		}
		
		/**
		 * @return the user name of the active user
		 */
		public function getUserName() {
			return $_SESSION["name"];
		}
		
		/**
		 * @param $seconds time in seconds
		 * @return inserted seconds as hour:minute:second
		 */
		public function his($seconds = -1) {
			if ($seconds == -1) {
				return date("H:i:s");
			}
			else {
				return date("H:i:s", $seconds);
			}
		}		
	}
?>
