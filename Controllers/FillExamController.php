<?php
	require_once("Controller.php");
	
	class FillExamController extends Controller {
		
		private $exam = array();
		
		public function FillExamController() {
			parent::Controller();
			
			$this->exam = $this->queryHandler->query("
				SELECT q.numberOfQuestions, q.title, e.examID, e.start, e.duration
				FROM exam e 
				INNER JOIN questionnaire q ON e.questionnaireID = q.questionnaireID
				WHERE e.examID='".$_GET["examID"]."'
				");
		}
		
		/**
		 * @return the number of questions
		 */
		public function getNumberOfQuestions() {
			return $this->exam[0]['numberOfQuestions'];
		}
		
		/**
		 * @return the exam id
		 */
		public function getExamID() {
			return $this->exam[0]['examID'];
		}
		
		/**
		 * @return the title of the questionnaire
		 */
		public function getTitle() {
			return $this->exam[0]['title'];
		}
		
		/**
		 * @return the end time of the exam
		 */
		public function getEnd() {
			return $this->exam[0]['start']+($this->exam[0]['duration']*60);
		}
	}
?>