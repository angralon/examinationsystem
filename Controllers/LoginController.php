<?php
	require_once("Controller.php");
	
	class LoginController extends Controller {
		
		private $loginMessage = "";
		
		public function LoginController() {
			session_start();
			parent::Controller(false, false);
			
			$sql = "SELECT userID,email,name,prof FROM user WHERE (email LIKE '".$_POST["email"]."') AND (password = '".$_POST["pw"]."')"; 
	
			$userData = $this->queryHandler->query($sql);
			
			//sets a session with the dependent usertype
			if (count($userData) > 0) 
			{		
				$_SESSION["userID"] = $userData[0]["userID"]; 
				$_SESSION["email"] = $userData[0]["email"]; 
				$_SESSION["name"] = $userData[0]["name"]; 
				$_SESSION["prof"] = $userData[0]["prof"]; 
			  
				if($this->isProf()){
					$userType = "professor";
				} 
				else {
					$userType = "student";
				}
			  
				$this->loginMessage = "You are now logged in as ".$userType." and ready to rock!";
			} 
			else 
			{
				$this->loginMessage = "Login failed, please make sure you typed your mail and password correctly.";
			}  
			
			$this->checkSession();
			
		}
		
		/**
		 * @return the login message
		 */
		public function getLoginMessage(){
			return $this->loginMessage;
		}
	}
?>