<?php
	require_once("Controller.php");
	
	class LogoutController extends Controller {
		
		private $logoutMessage = "";
		
		public function LogoutController() {
			parent::Controller(false,false);
			
			ob_start (); 
	
			session_start (); 
			session_unset (); 
			session_destroy (); 
			ob_end_flush (); 
			
			$this->logoutMessage = "you are now logged out";
			$this->checkSession();			
		}
		
		/**
		 * @return the logout message
		 */
		public function getLogoutMessage(){
			return $this->logoutMessage;
		}
	}
?>