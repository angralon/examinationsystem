<?php
	require_once("Controller.php");
	
	class SeeQuestionnairesController extends Controller {
		
		public function SeeQuestionnairesController() {
			parent::Controller();
		}
		
		/**
		 * Get the questionnaires for a professor
		 * @return them as array
		 */
		public function seeQuestionnaires(){
			try {
				return $this->queryHandler->query("SELECT * FROM questionnaire WHERE profID='".parent::getUser()."' ");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}
		}		
	}
?>