<?php
	require_once("Controller.php");
	
	class ActivateExamController extends Controller {
				
		public function ActivateExamController() {
			parent::Controller();
		}
		
		/**
		 * @return all modules of the logged in professor as an array
		 */
		public function getModules(){
			try {
				return $this->queryHandler->query("
					SELECT m.moduleID,m.moduleCode FROM module m 
					INNER JOIN profModule pM ON m.moduleID=pM.moduleID
					WHERE pM.profID='".$this->getUser()."' 
				");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}
		}
		
		/**
		 * @return the actual questionnaire title
		 */
		public function getQuestionnaireTitle() {
			try {
				return $this->queryHandler->query("SELECT title FROM questionnaire WHERE questionnaireID='".$_GET['questionnaireID']."' ");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}
		}
		
		/**
		 * @return the actual questionnaire id
		 */
		public function getQuestionnaireID() {
			return $_GET['questionnaireID'];
		}		
	}
?>