<?php
	require_once("Controller.php");
	
	class SeeExamsController extends Controller {
		
		public function SeeExamsController() {
			parent::Controller();
		}
		
		/**
		 * @return the exam id, the start time, the duration, the questionnaire id,
		 * the boolean active, the module code and the title from the exam
		 */
		public function seeExams(){
			try {
				return $this->queryHandler->query("
					SELECT e.examID, e.start, e.duration, e.questionnaireID, e.active, m.moduleCode, q.title
					FROM exam e 
					INNER JOIN module m ON e.moduleID = m.moduleID
					INNER JOIN profModule pM ON m.moduleID = pM.moduleID
					INNER JOIN questionnaire q ON e.questionnaireID = q.questionnaireID
					WHERE pM.profID='".$this->getUser()."'
					ORDER BY e.examID ASC
				");
			}
			catch (Exception $e) {
				echo "Query error!<br />".$e->getMessage();
				require_once('../Views/footer.php');	
				exit;
			}
		}
		
		/**
		 * @return the exam id, the start time, the duration, the questionnaire id,
		 * the module code and the title from the exam 
		 */
		public function studentSeeExams(){			
			try {
				return $this->queryHandler->query("
					SELECT e.examID, e.start, e.duration, e.questionnaireID, m.moduleCode, q.title 
					FROM exam e 
					INNER JOIN module m ON e.moduleID = m.moduleID
					INNER JOIN questionnaire q ON e.questionnaireID = q.questionnaireID
					INNER JOIN studentModule sM ON e.moduleID = sM.moduleID
					WHERE sM.studentID='".$this->getUser()."'
					AND e.active='1'
				");
			}
			catch (Exception $e) {
				echo "Query error!<br />";
				require_once('../Views/footer.php');	
				exit;
			}
		}
		
		/**
		 * @return the exam id of the logged in student
		 */
		public function studentSeeSubmittedExams(){		
			try {
				return $this->queryHandler->query("
					SELECT s.examID 
					FROM studentExam s 
					WHERE s.studentID='".$this->getUser()."'
				");
			}
			catch (Exception $e) {
				echo "Query error!<br />";
				require_once('../Views/footer.php');	
				exit;
			}
		}		
	}
?>