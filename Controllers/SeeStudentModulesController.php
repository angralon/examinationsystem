<?php
	require_once("Controller.php");
	
	class SeeStudentModulesController extends Controller {
		
		public function SeeStudentModulesController() {
			parent::Controller();
		}
		
		/** 
		 * Gets a student's modules from the database
		 * @return them as array 
		 */
		public function seeModules(){
			try {
				return $this->queryHandler->query("
					SELECT m.moduleID, m.moduleCode 
					FROM module m
					INNER JOIN studentModule sM ON m.moduleID = sM.moduleID
					WHERE sM.studentID = '".$this->getUser()."'	
				");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}
		}		
	}
?>