<?php
	require_once("Controller.php");
	
	class SeeResultsController extends Controller {
		
		public function SeeResultsController() {
			parent::Controller();
		}
		
		/**
		 * Gets the students that have already submitted the selected exam
		 * @return them as array
		 */
		public function seeStudents(){
			try {
				return $this->queryHandler->query("
					SELECT u.userID, u.name, u.email, e.active, e.examID, sE.submitTime
					FROM exam e
					INNER JOIN studentExam sE ON e.examID = sE.examID
					INNER JOIN user u ON sE.studentID = u.userID
					WHERE e.examID = '".$_REQUEST['examID']."'
				");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}		}	
		
		/**
		 * Gets the exam start time and duration,
		 * calculates the exam deadline
		 * @return the deadline
		 */
		public function getExamSubmitTime() {
			try {
				$exam = $this->queryHandler->query("
					SELECT start, duration
					FROM exam
					WHERE examID = '".$_REQUEST['examID']."'
				");
			}
			catch (Exception $e) {
				echo "Query error!";
				require_once('../Views/footer.php');	
				exit;
			}
			return $exam[0]['start']+($exam[0]['duration']*60);
		}
		
		/**
		 * @return the exam id
		 */	
		public function getExamID() {
			return $_REQUEST['examID'];
		}
	}
?>