<?php 
	class QueryHandler {
	
		private $connector;
		private $query;
	
		/**
		 * Opens the connection to the database
		 */
		public function QueryHandler() {			
			try {
				$this->connector = new Connector();
			}
			catch (Exception $e) {
				echo "Connection error!";
				require_once('../Views/footer.php');	
				exit;
			}		
		}
		
		/**
		 * @return the result as associative and indexed array
		 */
		public function query($query) {
	
			$c = $this->connector->get();
	    	$res = $c->query($query);
	
			$result = array();
			
			while($row = $res->fetch(PDO::FETCH_ASSOC)) 
			{
				$result[] = $row;
			}
				
			return $result;
		}
		
		/**
		 * @return the last insert id
		 */
		public function insert($query) {
			$c = $this->connector->get();
	    	$res = $c->query($query);
				
			return $c->lastInsertId();
		}
		
		/**
		 * @return the number of affected rows
		 */
		public function exec($query) {
			$c = $this->connector->get();
	    	$res = $c->exec($query);
				
			return $res;
		}
		
		/**
		 * closes the connection
		 */
		public function close(){
			$this->connector->close();
		}		
	}
?>