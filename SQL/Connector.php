<?php
	class Connector {
		
		/**
		 * Edit the database connection information in here
		 */
		private $dbConfig = array(
				'databaseType' => 'mysql',
				'host' => 'localhost',
				'user' => 'user',
				'password' => 'password',
				'database' => 'test', 
				'charset' => 'UTF-8', //disable for non utf-8
			);
		/* ------------------- */
		
		private $connection;
		
		/**
		 * Sets up the connection to the database
		 */
		public function Connector() {
	
			$this->connection = new PDO($this->dbConfig['databaseType'].":host=".$this->dbConfig['host'].";dbname=".$this->dbConfig['database'], $this->dbConfig['user'], $this->dbConfig['password']);
			
			if($this->dbConfig['charset'] == 'UTF-8')
			{
				$this->connection->query("SET NAMES utf8");
			}
			
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}	
		
		/**
		 * closes the database connection
		 */
		public function close() {
			$this->connection = null;
		}
		
		/**
		 * @return the PDO object
		 */
		public function get() {
			return $this->connection;
		}
	}
?>	
