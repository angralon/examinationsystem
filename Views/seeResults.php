<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeResultsController.php");
	$controller = new SeeResultsController();
	$header = new Header($controller);
	echo $header->getPageHead();
		
	$students = $controller->seeStudents();
	if(sizeof($students) > 0){
?>
		<table class="tableList">
			<tr>
				<th width="100">ID</th>
				<th width="100">Name</th>
				<th width="100">Mail</th>
				<th width="100">Late</th>
				<th width="100">Result</th>
		
			</tr>
<?php	
		$tolerance = 300; // Tolerance of submitting exams late in seconds
		
		foreach($students as $array) {
			
			if($controller->getExamSubmitTime() < ($array['submitTime']-$tolerance)) {
				$seconds = ($array['submitTime']-$controller->getExamSubmitTime());
				$late = '<img src="./images/late.png" border=0 height="12px" title="'.$seconds.' seconds too late">';
			}
			else {
				$late = '<img src="./images/notLate.png" border=0 height="12px" title="not late">';
			}
			
			if($array['active'] == 1) {	
				$result = "<a title=\"Deactivate this exam\" href=\"./submittedQueries.php?function=endExam&examID=".$array['examID']."\">X</a>";
			}
			else {
				$result = "<a href=\"./seeStudentResults.php?examID=".$controller->getExamID()."&userID=".$array['userID']."\">Result</a>";
			}
			
			echo "
				<tr>
					<td>".$array['userID']."</td>
					<td>".$array['name']."</td>
					<td>".$array['email']."</td>
					<td>".$late."</td>
					<td>".$result."</td>
				</tr>
			";
		}
		echo "</table>";
	}
	else {
		echo "No student has submitted an exam.";
	}
	
	require_once('footer.php');	
?>	