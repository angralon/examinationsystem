<?php	
	class Header {
		
		private $controller;
		
		public function Header($controller) {
			$this->controller = $controller;		
		}
		
		public function getPageHead() {
			
			/* Meta tags, page structure */
			$pageHead = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
				<head>
					<title>ExaminationSystem</title>
					<link rel="styleSheet" href="./css/general.css" type="text/css" media="screen" />
					<link rel="styleSheet" href="./css/forms.css" type="text/css" media="screen" />
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				</head>
				<body>
				<div id="container">
				<div id="center">
					<div class="headList">
					<ul class="headerLeft">
					<li><a href="index.php" >Home</a></li>
			';
						
			$logout = "";		
			if($this->controller->isLoggedIn()) {
				
				/* Menu for professors */
				$pageHeadLogged = "";
				if($this->controller->isProf()){
					$pageHeadLogged = '
						<li><a href="seeModule.php" >Modules</a></li>
						<li><a href="createQuestionnaire.php" >Create Questionnaire</a></li>
						<li><a href="seeQuestionnaires.php" >Questionnaires</a></li>
						<li><a href="seeExams.php" >Exams</a></li>			
					';
				}
				/* Menu for Students */
				else {
					$pageHeadLogged = '
						<li><a href="seeStudentModules.php" >My Modules</a></li>
						<li><a href="studentSeeExams.php" >Active Exams</a></li>
					';
				}
				
				$pageHead .= $pageHeadLogged;	
				
				/* Logout button */
				$logout .= '
					<span id="spanner"></span>
					<ul class="headerRight">
					<li><form action="logout.php"><input type="submit" class="submit" value="««" title="logout"></form></li>
					<li>Logged in as '.$this->controller->getUserName().'</li>
					</ul>
				';
			}
	
			$pageHead .= '
				</ul>
				'.$logout.'
				<hr style="clear:left;" />
				</div>
				<div id="content">
				<br />
			';
			
			return $pageHead;
		}
	
		public function getController() {
			return $this->controller;
		}
	}
?>
