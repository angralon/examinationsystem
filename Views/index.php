<?php 
	require_once("./header.php");
	require_once("../Controllers/IndexController.php");
	$controller = new IndexController();
	$header = new Header($controller);
	echo $header->getPageHead();
	
	if($header->getController()->isLoggedIn()) {
		echo '
			<p>Welcome to our ExaminationSystem!</p>
			<p>Go to your Exams to see what\'s going on.</p>
		';	
	}
	else {
		echo '
			<p>Welcome to our examination system!</p>
			<p>Please enter your login information.</p><br />
			<form action="login.php" method="post">
			
				<table>
					<tr>
						<td>UserName:</td><td><input type="text" name="email" /></td>
					</tr>
					<tr>
						<td>Password:</td><td><input type="password" name="pw" /></td><td><input type="submit" class="submit" value="✓" /></td>
					</tr>
				</table>	
					
			</form>
		';			
	}
				
	require_once("footer.php");	
?>	
