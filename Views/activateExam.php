<?php 
	require_once("./header.php");
	require_once("../Controllers/ActivateExamController.php");
	$controller = new ActivateExamController();
	$header = new Header($controller);
	echo $header->getPageHead();
?>

<h3>
<?php 
	$arr = $controller->getQuestionnaireTitle();
	echo $arr[0]['title']; 
?>
</h3>

<form action="submittedQueries.php" method="post">
	<input type="hidden" name="function" value="activateQuestionnaire" />
	<input type="hidden" name="questionnaireID" value="<?php echo $controller->getQuestionnaireID(); ?>" />
	Exam duration in minutes <input type="text" name="duration"><br />
	Module 
	<select name="moduleID" class="form">

<?php 
	foreach($controller->getModules() as $array) {
		echo "
			<option value='".$array['moduleID']."'>".$array['moduleCode']."</option>
		";
	}
?>

</select><br /><br />
<input type="submit" class="submit">
</form>

<?php
	require_once('footer.php');	
?>