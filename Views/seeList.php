<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeListController.php");
	$sL = new SeeListController();
	$header = new Header($sL);
	echo $header->getPageHead();
?>
<table class="tableList">
	<tr>
		<th>ID</th>
		<th>Mail</th>
		<th>Name</th>
	</tr>

<?php	
	foreach($sL->seeUsers() as $array) {
		echo "<tr>";
		foreach($array as $value) {
			echo "<td>".$value."</td>";
		}
		echo "</tr>";
	}
?>
</table>

<?php
	require_once("footer.php");	
?>	