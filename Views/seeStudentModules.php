<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeStudentModulesController.php");
	$controller = new SeeStudentModulesController();
	$header = new Header($controller);
	echo $header->getPageHead();
?>
<table class="tableList">
	<tr>
		<th>Module ID</th>
		<th>Module Code</th>
	</tr>

<?php	
	foreach($controller->seeModules() as $array) {
		echo "<tr>";
		echo "<td>".$array['moduleID']."</td>";
		echo "<td><a href=\"seeList.php?module=".$array['moduleID']."\" target=\"_self\">".$array['moduleCode']."</a></td>";
		echo "</tr>";
	}
?>
</table>

<?php
	require_once("footer.php");	
?>	