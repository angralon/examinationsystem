<?php 
	require_once("./header.php");
	require_once("../Controllers/SubmittedQueriesController.php");
	$controller = new SubmittedQueriesController();
	$header = new Header($controller);
	echo $header->getPageHead();
		
	echo $controller->getReturnMessage();
	
	require_once("footer.php");	
?>	