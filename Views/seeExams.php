<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeExamsController.php");
	$controller = new SeeExamsController();
	$header = new Header($controller);
	echo $header->getPageHead();
	
	
	$exams = $controller->seeExams();
	if(sizeof($exams) > 0) {
?>
	<table class="tableList">
		<tr>
			<th width="100">Exam Number</th>
			<th width="100">Module</th>
			<th width="100">Questionnaire</th>
			<th width="100">Start</th>
			<th width="100">Duration</th>
			<th width="100">Result</th>
		</tr>

<?php	
	foreach($exams as $array) {
		
		echo "
			<tr>
				<td>".$array['examID']."</td>
				<td>".$array['moduleCode']."</td>
				<td>".$array['title']."</td>
				<td>".$controller->his($array['start'])."</td>
				<td>".$array['duration']." min.</td>
			";
		
		if($array['active'] == 1) {	
		echo "
				<td>
					<a href=\"./submittedQueries.php?function=endExam&examID=".$array['examID']."\">
						<img src=\"./images/stopExam.png\" border=\"0\" height=\"12px\" title=\"Terminate this exam\">
					</a>
				</td>
			";
		}
		else {
		echo "	
				<td title=\"See result of this exam\"><a href=\"./seeResults.php?examID=".$array['examID']."\">Result</a></td>
			";
		}
		echo "</tr>";
	}
	echo "</table>";

}
else {
	echo 	"Please go to Questionnaires and select the one you would like to use for your exam.<br>
			You can define it by clicking on \"Activate this questionnaire\"";
}
require_once('footer.php');	
?>	