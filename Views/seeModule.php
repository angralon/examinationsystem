<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeListController.php");
	$sL = new SeeListController();
	$header = new Header($sL);
	echo $header->getPageHead();
?>
<table class="tableList">
	<tr>
		<th>Module ID</th>
		<th>Module Code</th>
	</tr>
	
<?php	
	foreach($sL->seeModules() as $array) {
		echo "<tr>";
		echo "<td>".$array['moduleID']."</td>";
		echo "<td><a href=\"seeList.php?module=".$array['moduleID']."\" target=\"_self\">".$array['moduleCode']."</a></td>";
		echo "</tr>";
	}
?>
</table>

<?php
	require_once("footer.php");	
?>	