<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeAnswersController.php");
	$controller = new SeeAnswersController();
	$header = new Header($controller);
	echo $header->getPageHead();
?>

<h3>
<?php 
	$arr = $controller->getQuestionnaireTitle();
	echo $arr[0]['title']; 
?>
</h3>

<p><a href="activateExam.php?questionnaireID=<?php echo $controller->getQuestionnaireID(); ?>">Activate this questionnaire</a></p>
<table class="tableList">
	<tr>
		<th width="200">Question Number</th>
		<th width="50">Answer</th>
	</tr>

<?php	
foreach($controller->seeAnswers() as $array) {
	
	echo "
		<tr>
			<td>".($array['questionNr']+1)."</td>
			<td>".$array['correctAnswer']."</td>
		</tr>
	";
}
?>
</table>

<?php
	require_once('footer.php');	
?>