<?php 
	require_once("./header.php");
	require_once("../Controllers/LogoutController.php");
	$controller = new LogoutController();
	$header = new Header($controller);
	echo $header->getPageHead();
	
	echo $controller->getLogoutMessage();
	echo '<br /><br /><form action="index.php"><input type="submit" value="Home"></form>';	
		
	require_once("footer.php");	
?>	