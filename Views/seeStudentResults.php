<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeStudentResultsController.php");
	$controller = new SeeStudentResultsController();
	$header = new Header($controller);
	echo $header->getPageHead();
?>

<h3>
<?php 
	echo $controller->getQuestionnaireTitle()." from ".$controller->getStudentName()." - Exam ID: ".$controller->getExamID(); 
?>
</h3>

<p>
<?php 
	echo '<a href="generatePDF.php?userID='.$controller->getStudent().'&examID='.$controller->getExamID().'">Create PDF</a>';
?>
</p>

<table class="tableList">
	<tr>
		<th width="200">Question Number</th>
		<th width="50">Student answer</th>
		<th width="50">Correct answer</th>
		<th width="50">Points</th>
	</tr>
<?php	
	foreach($controller->seeAnswers() as $array) {
		
		echo "
			<tr>
				<td>".($array['questionNr']+1)."</td>
				<td>".$array['studentAnswer']."</td>
				<td>".$array['correctAnswer']."</td>
				<td>".$array['points']."</td>
			</tr>
		";
	}
?>
</table>

<?php
	echo "
		<h2>Overall points: ".$controller->getAddedPoints()." / ".$controller->getMaxPoints()."</h2>
		<h2>Mark: ".$controller->getStudentMark()."</h2>
	";
	
	require_once('footer.php');	
?>	