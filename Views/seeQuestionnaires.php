<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeQuestionnairesController.php");
	$controller = new SeeQuestionnairesController();
	$header = new Header($controller);
	echo $header->getPageHead();
?>
<table class="tableList">
	<tr>
		<th width="90">ID</th>
		<th width="200">Title</th>
		<th width="40">Questions</th>
	</tr>

<?php	
	foreach($controller->seeQuestionnaires() as $array) {
		
		$link = "<a href=\"seeAnswers.php?questionnaireID=".$array['questionnaireID']."\" target=\"_self\">";
		echo "
			<tr>
				<td>".$link.$array['questionnaireID']."</a></td>
				<td>".$link.$array['title']."</a></td>
				<td>".$link.$array['numberOfQuestions']."</a></td>
			</tr>
		";
	}
?>
</table>

<?php
	require_once('footer.php');	
?>	