<?php 
	require_once("./header.php");
	require_once("../Controllers/CreateQuestionnaireController.php");
	$controller = new CreateQuestionnaireController();
	$header = new Header($controller);
	echo $header->getPageHead();
	
	if($controller->getNumberOfQuestions() > 0) {
		echo ' 
			<h2>'.$controller->getTitle().'</h2>
			<form action="submittedQueries.php" method="post" >
			<input type="hidden" name="function" value="storeQuestionnaire" />
			<input type="hidden" name="numberOfQuestions" value="'.$controller->getNumberOfQuestions().'" />
			<input type="hidden" name="title" value="'.$controller->getTitle().'" />
		';
	
		for($i = 0; $i < $controller->getNumberOfQuestions(); $i++)	{
			echo '
			<h3>Question '.($i+1).'</h3>
				<input type="radio" name="q'.$i.'" value="1">1
				<input type="radio" name="q'.$i.'" value="2">2
				<input type="radio" name="q'.$i.'" value="3">3
				<input type="radio" name="q'.$i.'" value="4">4
			<hr style="clear:left;"/>
			';
		}
		echo '
			<br />
			<input type="submit" class="submit" value="✓" />
			</form>
		';
	} 
	else
	{
		echo '
			Please enter the number of questions and a title for the questionnaire: <br /><br />
			<form action="createQuestionnaire.php" method="get" >
			<table>
				<tr>
					<td>Number of questions:</td><td><input type="text" name="numberOfQuestions" /></td>
				</tr>
				<tr>
					<td>Title:</td><td><input type="text" name="title" value="MyQuestionnaire"/></td>
				</tr>
				<tr>
					<td>&nbsp;</td><td><input type="submit" class="submit" value="✓" /></td>
				</tr>
			</table>
			</form>
		';
	}
	
	require_once('footer.php');	
?>	