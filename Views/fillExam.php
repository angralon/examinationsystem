<?php 
	require_once("./header.php");
	require_once("../Controllers/FillExamController.php");
	$controller = new FillExamController();
	$header = new Header($controller);
	echo $header->getPageHead();
	
	echo ' 
		<h2>Exam of '.date("d:m:Y").': '.$controller->getTitle().'</h2>
		<form action="submittedQueries.php" method="post" >
		<input type="hidden" name="function" value="fillExam" />
		<input type="hidden" name="numberOfQuestions" value="'.$controller->getNumberOfQuestions().'" />
		<input type="hidden" name="examID" value="'.$controller->getExamID().'" />
	';
	
	for($i = 0; $i < $controller->getNumberOfQuestions(); $i++)	{
		echo '
		<h4>Question '.($i+1).'</h4>
			<input type="radio" name="q'.$i.'" value="1">1
			<input type="radio" name="q'.$i.'" value="2">2
			<input type="radio" name="q'.$i.'" value="3">3
			<input type="radio" name="q'.$i.'" value="4">4
			<input type="radio" name="q'.$i.'" value="0">No answer
		<hr style="clear:left;"/>
		';
	}
	echo '
		<br />
		<h2 style="color:darkred">Deadline: '.$controller->his($controller->getEnd()).' !</h2>
		<input type="submit" class="submit" value="✓" />
		</form>
	';
	
	require_once('footer.php');	
?>	