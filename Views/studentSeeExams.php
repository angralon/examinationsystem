<?php 
	require_once("./header.php");
	require_once("../Controllers/SeeExamsController.php");
	$controller = new SeeExamsController();
	$header = new Header($controller);
	echo $header->getPageHead();
	
	$exams = $controller->studentSeeExams();
	if (sizeof($exams)>0) {
?>
		<table class="tableList">
			<tr>
				<th width="100">Exam Number</th>
				<th width="100">Module</th>
				<th width="100">Questionnaire</th>
				<th width="100">Start</th>
				<th width="100">Duration</th>
			</tr>
<?php	
		foreach($exams as $array) {
			
			$done = false;
			foreach ($controller->studentSeeSubmittedExams() as $key) {
				//echo $array['examID']." ".$key['examID']."<br>";
				if($array['examID'] == $key['examID']) {
					$done = true;
				}
			}
			if (!$done) {
				
				echo "
					<tr>
						<td><a href='./fillExam.php?examID=".$array['examID']."'>".$array['examID']."</a></td>
						<td><a href='./fillExam.php?examID=".$array['examID']."'>".$array['moduleCode']."</a></td>
						<td><a href='./fillExam.php?examID=".$array['examID']."'>".$array['title']."</a></td>
						<td><a href='./fillExam.php?examID=".$array['examID']."'>".$controller->his($array['start'])."</a></td>
						<td><a href='./fillExam.php?examID=".$array['examID']."'>".$array['duration']." min.</a></td>
					</tr>
				";
			}
		}
		echo "</table>";
	}
	else {
		echo "You have no active exams for the moment.";
	}
		
	require_once('footer.php');	
?>	